//
//  Changeset.swift
//  ChatCore
//
//  Created by George Kiriy on 18/05/2018.
//  Copyright © 2018 sMediaLink. All rights reserved.
//

public struct Changeset {
    public init(
        deleted: [Int],
        inserted: [Int],
        updated: [Int]
    ) {
        self.deleted = deleted
        self.inserted = inserted
        self.updated = updated
    }

    /// the indexes in the collection that were deleted
    public let deleted: [Int]

    /// the indexes in the collection that were inserted
    public let inserted: [Int]

    /// the indexes in the collection that were modified
    public let updated: [Int]
}
