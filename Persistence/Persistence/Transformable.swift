//
//  Transformable.swift
//  Chat
//
//  Created by Pavel Lavrik on 08.06.2018.
//  Copyright © 2018 sMediaLink. All rights reserved.
//

import Foundation
import RealmSwift

public protocol ManagedTransformable {
    associatedtype ManagedType: Object, PlainTransformable where ManagedType.PlainType == Self
    var managedObject: ManagedType { get }

    init(managed object: ManagedType)
}

public protocol PlainTransformable {
    associatedtype PlainType: ManagedTransformable where PlainType.ManagedType == Self

    var plainObject: PlainType { get }

    init(plain object: PlainType)

    @discardableResult
    func set(with plain: PlainType) -> Self
}

public extension ManagedTransformable {
    public init(required object: ManagedType?) {
        guard let obj = object else {
            fatalError("Required object not found")
        }
        self.init(managed: obj)
    }

    public init?(optional object: ManagedType?) {
        guard let obj = object else {
            return nil
        }
        self.init(managed: obj)
    }
}

public extension RawRepresentable {
    public init(required object: RawValue) {
        guard let decoded = Self(rawValue: object) else {
            fatalError("Failed to decode raw value")
        }

        self = decoded
    }
}
